// https://github.com/auth0/jwt-decode
// import decode from 'jwt-decode';
// Application > Local storage
export default {
  namespaced: true,
  state:{
    user: {},
    ID_TOKEN_KEY: '',
    ACCESS_TOKEN_KEY: ''
  },
  getters:{
    getUserInfo(){
      return JSON.parse(localStorage.getItem("user"));
    },
    getIdToken() {
      return localStorage.getItem(ID_TOKEN_KEY);
    },
    getAccessToken() {
      return localStorage.getItem(ACCESS_TOKEN_KEY);
    },
  },
  mutations:{
    setUserInfo(state){
      localStorage.setItem("user", JSON.stringify(state.user));
    },
    setIdToken(state) {
      let idToken = getParameterByName('id_token');
      localStorage.setItem(ID_TOKEN_KEY, idToken);
    },
    setAccessToken() {
      let accessToken = getParameterByName('access_token');
      localStorage.setItem(ACCESS_TOKEN_KEY, accessToken);
    },
    
    clearIdToken() {
      localStorage.removeItem(ID_TOKEN_KEY);
    },
    clearAccessToken() {
      localStorage.removeItem(ACCESS_TOKEN_KEY);
    },
    getTokenExpirationDate(encodedToken) {
      const token = decode(encodedToken);
      if (!token.exp) { return null; }
    
      const date = new Date(0);
      date.setUTCSeconds(token.exp);
    
      return date;
    },
    isTokenExpired(token) {
      const expirationDate = getTokenExpirationDate(token);
      return expirationDate < new Date();
    }
  },
  actions: {
    getParameterByName(name) {
      let match = RegExp('[#&]' + name + '=([^&]*)').exec(window.location.hash);
      return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    },
    logout() {
      clearIdToken();
      router.go('/');
    },
    requireAuth(to, from, next) {
      if (!isLoggedIn()) {
        next({
          path: '/',
          query: { redirect: to.fullPath }
        });
      } else {
        next();
      }
    },
    isLoggedIn({commit, dispatch}) {
      const idToken = getIdToken();
      return !!idToken && !isTokenExpired(idToken);
    },
    apiCall({commit, getters}){
      let apiClient = axios.create();
      apiClient.interceptors.request.use(config => {
        const token = getters.getAccessToken
        config.headers.Authorization = `Bearer ${token}`;
        return config
    })
    }
  }
}