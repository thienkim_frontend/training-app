
import EventService from '@/services/EventService.js'

export default {
    namespaced: true,
    state: {
      events: [],
      event: {},
      eventsTotal: 0,
      perPage: 5
    },
    getters: {
      getEventById: state => id => {
        return state.events.find(event => event.id === id);
      },
      getEventByStatus: (state, getters) => keyword => {
        return state.events.filter( event => event.status === keyword)
      }
    },
    mutations: {
      ADD_EVENT(state, event){
        state.events.push(event)
      },
      UPDATE_EVENT(state, event){
        let updatedIndex = state.events.findIndex(obj => obj.id === event.id);
        // make new object of updated object.   
        let updatedProjects = [
          ...state.events.slice(0, updatedIndex),
          event,
          ...state.events.slice(updatedIndex + 1),
        ];
        console.log(state.events)
        console.log(updatedProjects)
      },
      DELETE_EVENT(state, event){
        let index = state.events.findIndex(obj => obj.id === event.id)
        if (index !== -1) state.events.splice(index, 1);
      },
      SET_EVENT(state, event){
        state.event = event
      },
      SET_EVENTS(state, events){
        state.events = events
      },
      SET_EVENTS_TOTAL(state, eventsTotal){
        state.eventsTotal = eventsTotal
      }
    },
    actions: {
      createEvent({commit, dispatch}, event){
        return EventService.postEvent(event)
        .then((response) => {
          commit('ADD_EVENT', event)
          console.log(JSON.stringify(response, 0, 2) )
          const notification = {
              type: 'success',
              message: 'Your event has been created (' + event.name + ' )'
          }
          dispatch('notification/add', notification, {root: true})
        })
        .catch(error => {
            const notification = {
                type: 'error',
                message: 'There was a problem creating your event' + error.message
            }
            dispatch('notification/add', notification, {root: true})
            throw error
        })
      },
      updateEvent({commit, dispatch}, event){
        return EventService.putEvent(event)
        .then(() => {
          console.log(event)
          commit('UPDATE_EVENT', event)
          const notification = {
              type: 'success',
              message: 'Your event has been updated'
          }
          dispatch('notification/add', notification, {root: true})
        })
        .catch(error => {
            const notification = {
                type: 'error',
                message: 'There was a problem updating your event' + error.message
            }
            dispatch('notification/add', notification, {root: true})
            throw error
        })
      },
      fetchEvents({commit, dispatch, state}){
        return EventService.getEvents()
          .then(response => {
            commit('SET_EVENTS_TOTAL', parseInt(response.headers['x-total-count']))
            commit('SET_EVENTS', response.data)
            let notification = {
              type: 'success',
              message: 'Your data is fetched sucessful (' + JSON.stringify(response.data.length) + " items)"
            }
            dispatch('notification/add', notification, {root: true})
            return response.data;
          })
          .catch(error => {
            const notification = {
              type: 'error',
              message: 'There was a problem fetching events: ' + error.message
            }
            dispatch('notification/add', notification, { root: true })
          })
      },
      fetchEventsPagination({commit, dispatch, state}, {page}){
        return EventService.getEventsPagination(state.perPage, page)
          .then(response => {
            commit('SET_EVENTS_TOTAL', parseInt(response.headers['x-total-count']))
            commit('SET_EVENTS', response.data)
          })
          .catch(error => {
            const notification = {
              type: 'error',
              message: 'There was a problem fetching events: ' + error.message
            }
            dispatch('notification/add', notification, { root: true })
          })
      },
      fetchEvent({commit, getters}, id){
        var event = getters.getEventById(id)
        if(event){
          commit('SET_EVENT', event)
          return event
        }else{
          return EventService.getEvent(id)
            .then(response => {
              commit('SET_EVENT', response.data)
              return response.data
            })
            .catch(error => {
                const notification = {
                  type: 'error',
                  message: 'There was a problem fetching event: ' + error.message
                }
                dispatch('notification/add', notification, { root: true })
              })
        }
      },
      delEvent({commit, getters}, id){
        var event = getters.getEventById(id)
        if(event){
          commit('DELETE_EVENT', event)
          console.log(event)
          return EventService.delEvent(id)
            .then(response => {
              let notification = {
                type: 'success',
                message: 'Your event has been deleted' + response.status
              }
              dispatch('notification/add', notification, {root: true})
            })
            .catch(error => {
              let notification = {
                type: 'error',
                message: 'There was a problem deleting event: ' + error.message
              }
              dispatch('notification/add', notification, { root: true })
            })
        }
      },
    }
  }
  


  