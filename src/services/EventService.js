import axios from 'axios'
import NProgress from 'nprogress'
import { getAccessToken } from '@/services/auth';

const apiClient = axios.create({
    baseURL: 'http://localhost:3000',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
    timeout: 10000 // Throw error if API call takes longer than 10 seconds
})
// apiClient.interceptors.request.use(config => {
//     NProgress.start()
//     return config
// })
// apiClient.interceptors.response.use(response => {
//     NProgress.done()
//     return response
// })
export default{
    getEventsPagination(perPage, page){
        return apiClient.get('/staffs?_limit=' + perPage + '&_page=' + page)
    },
    getEvents(){
        return apiClient.get('/staffs')
    },
    getEvent(id){
        return apiClient.get('/staffs/' + id)
    },
    postEvent(event) {
        return apiClient.post('/staffs', event)
    },
    putEvent(event){
        return apiClient.put('/staffs/' + event.id, event)
    },
    delEvent(id){
        return apiClient.delete('/staffs/' + id)
    }
}