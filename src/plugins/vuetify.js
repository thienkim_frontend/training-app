import Vue from 'vue'
import Vuetify, { VLayout } from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

/* Error: Unknown custom element v-layout when using v-data-iterator
 * When using the vuetify-loader, there are a few scenarios which will require manual importing of components.
 * https://vuetifyjs.com/en/framework/a-la-carte#loader-limitations 
 */
Vue.use(Vuetify, {
  iconfont: 'md',
  components: {
    VLayout
  }
})
