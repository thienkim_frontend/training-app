import Vue from 'vue'
import Vuex from 'vuex'
import event from "@/modules/event.js";
import notification from "@/modules/notification.js";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    event,
    notification
  },
  state: {
    branchs: [
      "Tanca trụ sở",
      "Tanca Võ Thị Sáu",
      "Tanca Hai Bà Trưng",
    ],
    departments: [
      "Giám đốc",
      "Kinh doanh",
      "Pha chế",
      "Bếp"
    ],
    positions: [
      "Quản lý",
      "Quản lý Chi Nhánh",
      "Quản lý Vùng",
      "Nhân viên"
    ],
    statuses: [
      { val: false, label: "Nghỉ việc"},
      { val: true, label: "Đang làm việc"}
    ]
  },
  mutations: {

  },
  actions: {

  }
})
