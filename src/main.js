import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import store from './store'
import router from './router'
import Meta from 'vue-meta'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import Vuelidate from 'vuelidate';
import VuelidateErrorExtractor, {templates} from "vuelidate-error-extractor";
import VeeValidate from 'vee-validate';

import BootstrapVue from 'bootstrap-vue'

Vue.use(VeeValidate);
Vue.use(Meta)
Vue.use(BootstrapVue);
Vue.use(Vuelidate);
Vue.use(VuelidateErrorExtractor, {
  i18n: false,
  attributes: {
    name: 'Tên nhân viên',
    phone: 'Số điện thoại',
    branch: 'Chi nhánh',
    department: 'Phòng ban',
    position: 'Nhóm truy cập'
  },
})
Vue.component("form-group", templates.singleErrorExtractor.foundation6)

Vue.config.productionTip = false

const requireComponent = require.context(
  // The relative path of the components folder
  './components',
  // Whether or not to look in subfolders
  false,
  // The regular expression used to match base component filenames
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  // Get component config
  const componentConfig = requireComponent(fileName)

  // Get PascalCase name of component
  const componentName = upperFirst(
    camelCase(
      // Gets the file name regardless of folder depth
      fileName
      .split('/')
      .pop()
      .replace(/\.\w+$/, '')
    )
  )


  // Register component globally
  Vue.component(
    componentName,
    // Look for the component options on `.default`, which will
    // exist if the component was exported with `export default`,
    // otherwise fall back to module's root.
    componentConfig.default || componentConfig
  )
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
